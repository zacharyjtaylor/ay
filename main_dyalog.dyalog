⍙ ← {4 5::↓⍺∘.∇ ⍵⋄⍺←⊢⋄⍺ ⍺⍺ ⍵}

POSSIBLE ← {(~∨\⍵='⍝')∧~(¯1⌽o)∧o←≠\⍵=''''}

CHARACTER ← {
 ⍵
}

TRANSPILE ← {W←⍵⋄{M[⍵]:CHARACTER W[⍵]⋄W[⍵]}¨⍳⍴M←POSSIBLE W}

RUN ← {⍎∊TRANSPILE ⍵}