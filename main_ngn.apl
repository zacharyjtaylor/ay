⍝Note to self: understand WTF I'm doing here! (Never leave code unattended that you tend to use)
MACRO ← {⍺,'←',⍵,'⍠{w←⍵⋄0=⍴⍴⍵:⍺',⍵,'⍵⋄0=⍴⍴⍺:⍺',⍵,'⍵⋄((⍴⍴⍺)≡⍴⍴⍵)∧(⍴⍺)≡⍴⍵:⍺',⍵,'⍵⋄{⍵ ',⍺,' w}¨⍺}⋄'}

PREFIX ← ''

POSSIBLE ← {(~∨\⍵='⍝')∧~(¯1⌽o)∧o←≠\⍵=''''}

CHARACTER ← {
 ⍵
}

TRANSPILE ← {W←⍵⋄{M[⍵]:CHARACTER W[⍵]⋄W[⍵]}¨⍳⍴M←POSSIBLE W}
RUN ← {⍎∊PREFIX,TRANSPILE ⍵}